FROM ruby:2.5-alpine

RUN apk update
RUN apk add --no-cache build-base postgresql postgresql-dev libpq

WORKDIR /app
COPY . /app
RUN apk add --no-cache build-base sqlite sqlite-dev sqlite-libs
RUN gem install sqlite3 sinatra haml data_mapper dm-sqlite-adapter
RUN apk del --purge build-base sqlite-dev
RUN bundle install -j $(nproc) --quiet

EXPOSE 2300
ENTRYPOINT ["bundle", "exec"]
